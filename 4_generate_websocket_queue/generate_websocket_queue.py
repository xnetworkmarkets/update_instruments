import redis
import json
import ast
import datetime
import time
redis1=redis.StrictRedis(host='test-mrkt.j2jgvs.ng.0001.euc1.cache.amazonaws.com', port=6379,db=1)

def lambda_handler(event, context):
    ab=redis1.eval("local keys_trades = redis.call('keys', '*.trades') local keys_orderbook = redis.call('keys', '*.orderbook') local result = {} for idx, key in ipairs(keys_trades) do local res2={} table.insert(res2,key) table.insert(res2,redis.call('get', key)) table.insert(result, res2) end for idx, key in ipairs(keys_orderbook) do local res2={} table.insert(res2,key) table.insert(res2,redis.call('get', key)) table.insert(result, res2) end return result",0,"0")
    list_to_append=[]
    v=time.time()
    for i in ab:#
        k=ast.literal_eval(i[1].decode())
        tim=k['header']['updated_at']
        a=datetime.datetime(int(tim.split('-')[0]),int(tim.split('-')[1]),int(tim.split(' ')[0].split('-')[2]),int(tim.split(' ')[1].split(':')[0]),int(tim.split(' ')[1].split(':')[1]),int(tim.split(' ')[1].split(':')[2].split('.')[0])).timetuple()
        if(v-time.mktime(a)>600):
            list_to_append.append(i[0])

    redis1.delete('1_websocket_queue')
    if(list_to_append!=[]):
        redis1.rpush('1_websocket_queue',*list_to_append)
      
    return {
        'statusCode': 200,
        'body': {'status':'success'}}