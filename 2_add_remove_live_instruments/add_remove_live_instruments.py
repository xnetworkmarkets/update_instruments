import json
import redis
import ast

redis_index=redis.StrictRedis(host='test-mrkt.j2jgvs.ng.0001.euc1.cache.amazonaws.com', port=6379,db=1)
redis_change=redis.StrictRedis(host='test-mrkt.j2jgvs.ng.0001.euc1.cache.amazonaws.com', port=6379,db=2)
# Connect to the database
# Connect to the database

def add_remove(event, context):
    for k in redis_change.keys('*'):
        i=k.decode()
        if(i.split('.')[0]=='to_add'):
            j=i.split('.')[1]+'.'+i.split('.')[2]+'.'+i.split('.')[3]+'.'+i.split('.')[4]+'.'+i.split('.')[5]
            res=ast.literal_eval(redis_change.get(i).decode()) 
            redis_index.set(j,json.dumps(res))
            redis_change.delete(i)
        elif(i.split('.')[0]=='to_remove'):
            j=i.split('.')[1]+'.'+i.split('.')[2]+'.'+i.split('.')[3]+'.'+i.split('.')[4]+'.'+i.split('.')[5]
            redis_index.delete(j)
            redis_change.delete(i)
        
    return {
        'statusCode': 200,
        'body': {'status':'success'}}