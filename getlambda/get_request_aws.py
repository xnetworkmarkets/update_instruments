import requests
import time
import redis
import json

redis_db = redis.StrictRedis(host='localhost', port=6379, db=20)
api_endpoint_raw = "https://api.coingecko.com/api/v3/coins/markets?vs_currency=USD&order=market_cap_desc&per_page=250&page=pagenumber&sparkline=false"

while True:
    api_content = "initialization"
    currency_list =[]
    i = 0
    rates_ccy2 = {}
    currency_list_asset2 = ["BTC","USDT","ETH","TUSD","DAI","EURS","EOS","GUSD","PAX","USDC","EOSDT","BCH","BUSD","BNB","XRP","USDS","TRX","HT","HUSD","USDK","OKB","USD","EUR","JPY","GBP","XLM","XCHF","CNHT","CAD","DFT","KCS","NEO","NUSD"]

    print("Retreiving API Data")
    while api_content != []:

        api_endpoint= api_endpoint_raw.replace("pagenumber",str(i))
        api_content = requests.get(url = api_endpoint)
        api_content = api_content.json()

        #Load elements from Coingecko API
        for currencies in api_content:
            currency = {}
            currency["asset_details"] = currencies
            currency["asset_details"]['symbol'] = currencies['symbol'].upper()
            currency["asset_details"]['price_USD'] = currencies['current_price']
            del currency["asset_details"]['current_price']
            currency_list.append(currency)
            
            if (currency["asset_details"]['symbol'] in currency_list_asset2):
                rates_ccy2[currency["asset_details"]['symbol']] = currency["asset_details"]['price_USD']
                a = 0

        print("API call number "+str(i)+" processed") 
        i=i+1

    print(rates_ccy2)
    currency_list_processed =[]

    i=0
    print("Processing Data")
    #Process Data and push keys into the DB
    for currencies1 in currency_list:

        currencies1["exchange_rates"] = {}
        j=0

        try: price1 = float(currencies1["asset_details"]['price_USD'])
        except: price1 = 0

        for key in rates_ccy2:
            try: price2 = float(rates_ccy2[key])
            except: price2 = 0
            try: 
                exchange_rate = price1/price2
            except:
                exchange_rate = "N/A"
            currencies1["exchange_rates"][key]= exchange_rate
            j=j+1

        redis_db.set("asset_"+currencies1["asset_details"]['symbol'],json.dumps(currencies1))

        i=i+1

    time.sleep(15)
