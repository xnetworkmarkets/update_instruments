import json
import redis
import pymysql

redis_index=redis.StrictRedis(host='test-mrkt.j2jgvs.ng.0001.euc1.cache.amazonaws.com', port=6379,db=1)
redis_change=redis.StrictRedis(host='test-mrkt.j2jgvs.ng.0001.euc1.cache.amazonaws.com', port=6379,db=2)
name = "admin"
password = "I5wlDkpoGoGFUlo2poxD"
db_name = "currency_pair"
host="markets-1.car8rqrq2u9h.eu-central-1.rds.amazonaws.com"

def lambda_handler(event, context):
    for i in redis_change.keys('to_map.*'):
        i=i.decode()
        if(i.split('.')[2]=='options'):
            print(i)

    for i in redis_change.keys('to_map.*'):
        i=i.decode()
        if(i.split('.')[2]=='options'):
            if(i.split('.')[1]=='deribit'):
                cnx=pymysql.connect(host=host,user=name,passwd=password,db=db_name)
                cursor = cnx.cursor()
                query = "INSERT INTO options(pairs,Currency1,Currency2,deribit,ftx) " \
                        "VALUES(%s,%s,%s,%s,%s)"
                args = (i.split('.')[3]+'.USD', i.split('.')[3],'USD',i.split('.')[3],'')
                cursor.execute(query,args)
                cnx.commit()
                cnx.close()
            elif(i.split('.')[1]=='ftx'):
                cnx=pymysql.connect(host=host,user=name, passwd=password,db=db_name)
                cursor = cnx.cursor()
                query = "INSERT INTO options(pairs,Currency1,Currency2,deribit,ftx) " \
                        "VALUES(%s,%s,%s,%s,%s)"
                args = (i.split('.')[3]+'.USD', i.split('.')[3],'USD','',i.split('.')[3])
                cursor.execute(query,args)
                cnx.commit()
                cnx.close()
            redis_change.delete(i)
    
    return {
        'statusCode': 200,
        'body': {'status':'success'}}