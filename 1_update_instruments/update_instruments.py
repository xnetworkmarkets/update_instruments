import pymysql
import requests
import rds_config
import json
import redis
import uuid
import ast
import time
from datetime import datetime

redis_index=redis.StrictRedis(host='test-mrkt.j2jgvs.ng.0001.euc1.cache.amazonaws.com', port=6379,db=1)
redis_change=redis.StrictRedis(host='test-mrkt.j2jgvs.ng.0001.euc1.cache.amazonaws.com', port=6379,db=2)
# Connect to the database
# Connect to the database
host  = rds_config.host
name = rds_config.db_username
password = rds_config.db_password
db_name = rds_config.db_name
exclusion_list=list(json.loads(redis_change.get("0_to_exclude").decode()))

def request_instru(exchange,typ):
    liste_market=[]
    if(typ=='futures'):
        if(exchange=='ftx'):
            query='https://ftx.com/api/markets'
            r=requests.get(query).json()['result']
            print(r)
            for i in r:
                if(i['type']=='future' and i['name'].split('-')[1]!='MOVE'):
                    liste_market.append(i['name']) 
        elif(exchange=='bitmex'):
            query='https://www.bitmex.com/api/v1/instrument/active'
            r=requests.get(query).json()
            #print(r)
            for i in r:
                try:
                    liste_market.append(i['symbol'])
                except:
                    a=0;
        elif(exchange=='kraken'):
            query='https://www.cryptofacilities.com/derivatives/api/v3/instruments'
            r=requests.get(query).json()['instruments']
            for i in r:
                if(i['symbol'].split('_')[0]!='rr' and i['symbol'].split('_')[0]!='in'):
                    liste_market.append(i['symbol'])
        elif(exchange=='deribit'):
            query='https://deribit.com/api/v2/public/get_instruments?currency=BTC&expired=false&kind=future'
            r=requests.get(query).json()['result']
            for i in r:
                liste_market.append(i['instrument_name'])
            query='https://deribit.com/api/v2/public/get_instruments?currency=ETH&expired=false&kind=future'
            r=requests.get(query).json()['result']
            for i in r:
                liste_market.append(i['instrument_name'])
        elif(exchange=='binance'):
            query='https://fapi.binance.com/fapi/v1/exchangeInfo'
            r=requests.get(query).json()['symbols']
            for i in r:
                liste_market.append(i['symbol'].lower())
        elif(exchange=='huobi'):
            query='https://api.hbdm.com/api/v1/contract_contract_info'
            r=requests.get(query).json()['data']
            for i in r:
                liste_market.append(i['contract_code'])  
        elif(exchange=='okex'):
            query='https://www.okex.com/api/futures/v3/instruments'
            r=requests.get(query).json()
            for i in r:
                liste_market.append(i['instrument_id'])
        elif(exchange=='bitfinex'):
            query='https://api-pub.bitfinex.com/v2/tickers?symbols=ALL'
            r=requests.get(query).json()
            for i in r:
                if(i[0].split(':').length==2 and i[0].split(':')[1][3::]=='F0'):
                    liste_market.append(i[0])
    elif(typ=='spot'):
        if(exchange=='binance'):
            query='https://api.binance.com/api/v3/exchangeInfo'

            r=requests.get(query).json()['symbols']
            for i in r:
                liste_market.append(i['symbol'].lower())
        elif(exchange=='hitbtc'):
            query='https://api.hitbtc.com/api/2/public/symbol'
            r=requests.get(query).json()
            for i in r:
                liste_market.append(i['id'])
        elif(exchange=='huobi'):
            query='https://api.huobi.pro/v1/common/symbols'
            r=requests.get(query).json()['data']
            for i in r:
                liste_market.append(i['symbol'])  
        elif(exchange=='okex'):
            query='https://www.okex.com/api/spot/v3/instruments'
            r=requests.get(query).json()
            for i in r:
                liste_market.append(i['instrument_id'])
        elif(exchange=='bitfinex'):
            query='https://api-pub.bitfinex.com/v2/tickers?symbols=ALL'
            r=requests.get(query).json()
            for i in r:
                if(i[0][0]=='t'):
                    liste_market.append(i[0])
        elif(exchange=='kraken'):
            query='https://api.kraken.com/0/public/AssetPairs'
            r=requests.get(query).json()['result']
            for i in list(r):
                try:
                    liste_market.append(r[i]['wsname'])
                except:
                    a=0

        elif(exchange=='coinbase'):
            query='https://api.pro.coinbase.com/products'
            r=requests.get(query).json()
            for i in r:
                liste_market.append(i['id'])
        elif(exchange=='digifinex'):
            query='https://openapi.digifinex.com/v3/spot/symbols'
            r=requests.get(query).json()['symbol_list']
            for i in r:
                liste_market.append(i['symbol'])
        elif(exchange=='poloniex'):
            query='https://poloniex.com/public?command=returnTicker'
            r=requests.get(query).json()
            for i in list(r):
                liste_market.append(str(r[i]['id']))
        elif(exchange=='ftx'):
            query='https://ftx.com/api/markets'
            r=requests.get(query).json()['result']
            for i in r:
                if(i['type']=='spot' and  ('HEDGE'not in i['name']) and  ('MOON'not in i['name']) and  ('DOOM'not in i['name']) and  ('BULL'not in i['name']) and  ('BEAR'not in i['name'])):
                    liste_market.append(i['name']) 
    elif(typ=='options'):
        if(exchange=='deribit'):
            query='https://deribit.com/api/v2/public/get_instruments?currency=BTC&expired=false&kind=option'
            r=requests.get(query).json()['result']
            for i in r:
                liste_market.append(i['instrument_name'])
            query='https://deribit.com/api/v2/public/get_instruments?currency=ETH&expired=false&kind=option'
            r=requests.get(query).json()['result']
            for i in r:
                liste_market.append(i['instrument_name'])
        elif(exchange=='ftx'):
            query='https://ftx.com/api/markets'
            r=requests.get(query).json()['result']
            for i in r:
                if(i['type']=='future' and i['name'].split('-')[1]=='MOVE'):
                    liste_market.append(i['name'])       
                
    return(liste_market)





def get_pairs_db(exchange,typ):
    liste_market=[]
    cnx=pymysql.connect(host=host,
                        user=name,
                        passwd=password,
                        db=db_name)
    cursor = cnx.cursor()
    add_pr = ("SELECT "+exchange+ " from "+typ)
    cursor.execute(add_pr)
    myresult = cursor.fetchall()
    for i in myresult:
        a=''.join(i)
        if(a!=''):
            liste_market.append(a)
    cnx.close()
    return(liste_market)
    
    
def get_pairs_har(exchange,typ,ex_pair):
    liste_market=[]
    cnx=pymysql.connect(host=host,
                        user=name,
                        passwd=password,
                        db=db_name)
    cursor = cnx.cursor()
    add_pr = ("SELECT pairs from "+typ+" WHERE "+ exchange+ " LIKE '"+ ex_pair+"'")
    #print(add_pr)
    cursor.execute(add_pr)
    myresult = cursor.fetchall()
    for i in myresult:
        a=''.join(i)
        if(a!=''):
            liste_market.append(a)
    cnx.close()
    try:
        return(exchange+'.'+typ+'.'+liste_market[0])
    except:
        return(0)

list_combi=[['hitbtc','spot'],['binance','spot'],['huobi','spot'],['okex','spot'],['bitfinex','spot'],['kraken','spot'],['coinbase','spot'],['bittrex','spot'],['digifinex','spot'],['poloniex','spot'],['kucoin','spot'],['ftx','spot'],['ftx','options'],['deribit','options'],['bitmex','futures'],['kraken','futures'],['deribit','futures'],['binance','futures'],['binance','futures'],['ftx','futures']]#,['huobi','futures'],['okex','futures'],['bitfinex','futures']]
##['binance','futures'],['huobi','futures'],['okex','futures'],['bitfinex','futures']
def get_new_instru(exchange,typ):
    live_instru=request_instru(exchange,typ)
    ins_to_add=[]
    #print('instru to add')
    for i in live_instru:
        a=get_pairs_har(exchange,typ,i)
        if(a==0):
            ins_to_add.append(exchange+'.'+typ+'.'+i)
    return ins_to_add

def get_instru_to_remove(exchange,typ):
    db_pairs=get_pairs_db(exchange,typ)
    live_instru=request_instru(exchange,typ)
    ins_to_remove=[]
    #print('instru to remove')
    for i in db_pairs:
        if(i not in live_instru):
            ins_to_remove.append(exchange+'.'+typ+'.'+i)
    return ins_to_remove

def add_keys_to_remove(to_remove):
    current_keys=[]
    to_remove2=[]
    current_change=[]

    for i in redis_index.keys('*'):
        current_keys.append(i.decode())
    for i in redis_change.keys('*'):
        current_change.append(i.decode())

    for i in to_remove:
        new_key=get_pairs_har(i.split('.')[0],i.split('.')[1],i.split('.')[2])
        to_remove2.append(new_key)

    for i in current_keys:
        if(len(i.split('.'))>3):
            j=i.split('.')[0]+'.'+i.split('.')[1]+'.'+i.split('.')[2]+'.'+i.split('.')[3]
            if((j in to_remove2) and ('to_remove'+i not in current_change)):
                redis_change.set('to_remove.'+i,json.dumps({'status':'to_be_removed'}))
            #print('to_remove.'+i+'.orderbook')
                redis_change.set('to_remove.'+i,json.dumps({'status':'to_be_removed'}))

    return to_remove2


def add_keys_to_add():
    current_keys=[]
    to_add2=[]
    current_change=[]

    for i in redis_index.keys('*'):
        current_keys.append(i.decode())
    for i in redis_change.keys('*'):
        current_change.append(i.decode())

    for i in list_combi:
        instru=request_instru(i[0],i[1])
        for k in instru:
            res=get_pairs_har(i[0],i[1],k)
            if(res!=0 and res+'.trades' not in current_keys and res+'.trades' not in current_change and res!="huobi.spot.HydroProtocol.BTC" and res!="huobi.spot.HydroProtocol.ETH"):
                header={}
                header["id"]= str(uuid.uuid4())
                header["created_at"] = str(datetime.utcnow())
                header["updated_at"] = header["created_at"]
                if(len(res.split('.'))>3):
                    redis_change.set('to_add.'+res+'.trades',json.dumps({'header':header,'payload':{'exchange': res.split('.')[0], 'type': res.split('.')[1], 'currency1': res.split('.')[2], 'currency2': res.split('.')[3], 'feed': 'trades'}}))
                    redis_change.set('to_add.'+res+'.orderbook',json.dumps({'header':header,'payload':{'exchange': res.split('.')[0], 'type': res.split('.')[1], 'currency1': res.split('.')[2], 'currency2': res.split('.')[3], 'feed': 'orderbook'}}))
                #to_add2.append(k)
    return 0

def handler(event, context):
    inst={}
    to_be_mapped=[]
    to_remove=[]
    for i in list_combi:
        try:
            a=inst[i[1]]
        except:
            inst[i[1]]={}
        inst[i[1]][i[0]]={}
        inst[i[1]][i[0]]['to_add']=get_new_instru(i[0],i[1])
        for j in inst[i[1]][i[0]]['to_add']:
            if('to_map.'+j not in exclusion_list):
                to_be_mapped.append(j)
                redis_change.set('to_map.'+j,json.dumps({'status':'to map'}))

        to_remove+=get_instru_to_remove(i[0],i[1])
    b=add_keys_to_remove(to_remove)
    c=add_keys_to_add()
    return {
        'statusCode': 200,
        'body': {'to_add':to_be_mapped}}

